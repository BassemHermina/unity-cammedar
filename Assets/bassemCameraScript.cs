﻿
using UnityEngine;
using System.Collections;

public class bassemCameraScript : MonoBehaviour {

	// Use this for initialization
	/// ZOOM ///////////////////////////
	private float number;
	public float zoomSpeed = 55;
	//==


	/// DRAGGING ///////////////////////////
	public float dragSpeed = 0.1f;
	private Vector2 dragOrigin;
	private Vector3 dragOrigin2;
	private Vector2 dragNow;
	private Vector2 dragDelta;
	private Vector3 cameraPosOrigin;
	private bool dragging = false;

	public float walkSpeed = 0.1f;
	//==

	/// ORBITTING ///////////////////////////
	public  float lookSensitivity = 5;
	private float yRotation;
	private float xRotation;
	private float currentYRotation;
	private float currentXRotation;
	private float yRotationV;
	private float xRotationV;
	public  float lookSmoothDamp = 0.1f;
	//==


	void Start () {
	
	}

	// Update is called once per frame
	void Update () {

		/////////////////////////////////////////////////////////
		// based on https://www.youtube.com/watch?v=3JsuldsGuNw
		// Master Indie youtube channel
		//mouse look orbitting
		if (!dragging) {
			yRotation += Input.GetAxis ("Mouse X") * lookSensitivity;
			xRotation -= Input.GetAxis ("Mouse Y") * lookSensitivity;

			xRotation = Mathf.Clamp (xRotation, -90, 90);

			currentXRotation = Mathf.SmoothDamp (currentXRotation, xRotation, ref xRotationV, lookSmoothDamp);
			currentYRotation = Mathf.SmoothDamp (currentYRotation, yRotation, ref yRotationV, lookSmoothDamp);

			transform.rotation = Quaternion.Euler (currentXRotation, currentYRotation, 0);
		}
		///===================================


		///////////////////////////////////////////////////////////
		// Zooming in and out
		number = Input.GetAxis("Mouse ScrollWheel");
		gameObject.transform.Translate (Vector3.forward * Time.deltaTime * number * zoomSpeed);
		//=================================

		if (Input.GetKey (KeyCode.A))
			gameObject.transform.Translate (Vector3.left * Time.deltaTime * zoomSpeed * walkSpeed);
		if (Input.GetKey (KeyCode.D))
			gameObject.transform.Translate (Vector3.right * Time.deltaTime * zoomSpeed * walkSpeed);
		if (Input.GetKey (KeyCode.W))
			gameObject.transform.Translate (Vector3.up * Time.deltaTime * zoomSpeed * walkSpeed);
		if (Input.GetKey (KeyCode.S))
			gameObject.transform.Translate (Vector3.down * Time.deltaTime * zoomSpeed * walkSpeed);
		

		///////////////////////////////////////////////////////////
		//dragging
		if (Input.GetMouseButtonDown (0)) {
			dragOrigin = Input.mousePosition;
			dragging = true;
			cameraPosOrigin = gameObject.transform.position;
			return;
		}

		//3'aleban di lazem tt7at fl a5er , 3ashan el return di hat2fel functio
		//update kolha asln
		if (!Input.GetMouseButton (0)) {
			dragging = false;
			return; 
		}
		/*
		dragNow = Input.mousePosition;
		dragDelta = dragNow - dragOrigin;
		gameObject.transform.position = new Vector3 (cameraPosOrigin.x + (dragDelta.x*0.03f) , cameraPosOrigin.y  + (dragDelta.y*0.03f), cameraPosOrigin.z);
		*/
		/*
		Vector3 pos = Camera.main.ScreenToViewportPoint (Input.mousePosition - dragOrigin2);
		Vector3 move = new Vector3 (pos.x * dragSpeed,0,  pos.y * dragSpeed);

		transform.Translate (move, Space.World);
		*/
		//==========================================


	}
		
}
