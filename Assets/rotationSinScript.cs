﻿using UnityEngine;
using System.Collections;

public class rotationSinScript : MonoBehaviour {

	private Quaternion _startRotation;
	public float animationSpeed = 3.0f;  // lma ba7ot el variable byb2a asra3 msh 3ref leeh :/ akid 7aga liha 3laka bl float wel double
	public float range = 25;
	// Use this for initialization
	void Start () {
		_startRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {

		//new rotation = start rotation + (*) a small rotation part depending on sin(time)
		//														sin (time *speed) * range
		transform.rotation = _startRotation * Quaternion.Euler(Mathf.Sin(Time.time*3.0f)*10.0f, Mathf.Sin(Time.time*3.0f)*10.0f, 0.0f);
	}
}
